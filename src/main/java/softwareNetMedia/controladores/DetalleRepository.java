package softwareNetMedia.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import softwareNetMedia.modelo.DetalleOrden;

/**
 * Este método se utiliza para hacer una busqueda en la base de datos y extraer el external id_de la cuenta
 */
public interface DetalleRepository extends CrudRepository<DetalleOrden, Integer> {
    @Query("SELECT c from Cuenta c WHERE c.external_id = ?1")
    DetalleOrden findByExternal_id(String external_id);

    

    
    
}
