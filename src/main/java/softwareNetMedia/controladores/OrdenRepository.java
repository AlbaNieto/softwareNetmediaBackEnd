package softwareNetMedia.controladores;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import softwareNetMedia.modelo.Orden;
import softwareNetMedia.modelo.enums.EstadoFactura;
/**
 * Consultas query para obtener ordenes
 */
public interface OrdenRepository extends CrudRepository<Orden, Integer>{
    /**
     * Buscar orden por su external id
     * @param external_id External id de la orden
     * @return Objeto orden correspondiente al external id
     */
    @Query("SELECT o from Orden o WHERE o.external_id = ?1")
    Orden findByExternal_id(String external_id);
    /**
     * Buscar orden entre un rango de fecha
     * @param fechaInicio Fecha inicial del rango
     * @param fechaFinal Fecha final del rango
     * @return Lista de ordenes que se encuentran en el rango de fechas
     */
    @Query("SELECT o from Orden o WHERE o.fechaEmision BETWEEN ?1 and ?2")
    List<Orden>findByFechaEmisionBetween(Date fechaInicio, Date fechaFinal);
    @Query("SELECT o from Orden o WHERE o.estado =?1")
    /**
     * metodo para buscar ordenes por estado
     * @param estado
     * @return lista de ordenes con el correspondiente estado
     */
    List<Orden> findByEstado(EstadoFactura estado);
    @Query("SELECT o from orden o WHERE o.fechaEmision =?1")
    List<Orden> findByFechaEmision(Date fechaEmision);
    @Query("SELECT o from Orden o WHERE o.equipoElectronico.persona.identificacion =?1")
    /**
     * metodo para buscar ordenes por identificacion del cliente
     * @param identificacion
     * @return lista de ordenes del correspondiente cliente
     */
    List<Orden> buscarPorIdentificacion(String identificacion);
    @Query("SELECT o from Orden o WHERE o.equipoElectronico.persona.nombres =?1")
    /**
     * metodo para buscar ordenes por nombre del cliente
     * @param nombres
     * @return lista de ordenes del correspondiente cliente
     */
    List <Orden> buscarPorNombres(String nombres);

    @Query("SELECT o FROM orden o WHERE o.nroFactura = ?1")
    Orden findByNroFactura(Long nroFactura);

}

