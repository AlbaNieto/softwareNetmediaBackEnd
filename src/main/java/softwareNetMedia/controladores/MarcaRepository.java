package softwareNetMedia.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import softwareNetMedia.modelo.Marca;

public interface MarcaRepository extends CrudRepository<Marca, Integer> {
    /**
     * Este método se utiliza para buscar el nombre de la marca en la base de datos
     * @param nombre_marca
     * @return
     */
    @Query("SELECT l from marca l WHERE l.nombre = ?1")
    Marca findByNombre_Marca(String nombre_marca);

}
