package softwareNetMedia.controladores;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import softwareNetMedia.modelo.EquipoElectronico;
import softwareNetMedia.modelo.Persona;

public interface EquipoRepository extends CrudRepository<EquipoElectronico, Integer>{
    /**
     * Este método se utiliza para hacer una busqueda en la base de datos y extraer el external id_de la equipoElectronico
     * @param external_id
     * @return
     */
    @Query("SELECT p from equipoElectronico p WHERE p.external_id = ?1")
    List<EquipoElectronico> findByExternal_id(String external_id);
    

    /**
     * Este método se utiliza para hacer una busqueda en la base de datos y extraer el external id_de la equipoElectronico
     * @param external_id
     * @return
     */
    @Query("SELECT p from equipoElectronico p WHERE p.persona.external_id = ?1")
    List<EquipoElectronico> buscarExternal_id(String external_id);

    List<EquipoElectronico> findByPersona(Persona persona);
    /**
     * 
     * @param external_id
     * Este método se utiliza para hacer una busqueda en la base de datos y extraer el external id_de la equipoElectronico
     * @return
     */
    @Query("SELECT p from equipoElectronico p WHERE p.external_id = ?1")
    EquipoElectronico findByExternal_idAlt(String external_id);

    /**
     * Este método se utiliza para hacer una busqueda en la base de datos y ordenar  equipoElectronico
     * @return
     */
    @Query(value= "SELECT * FROM equipo_electronico ORDER BY id",
        countQuery = "SELECT count(*) FROM equipo_electronico",
            nativeQuery = true)
    List<EquipoElectronico> findByIdOrderByDesc();

    //@Query(value= "SELECT f FROM equipoElectronico as f ORDER BY f.id")
   //y List<EquipoElectronico> findByIdOrderByDesc();


    //@Query("SELECT f from equipoElectronico")
    //List<EquipoElectronico> findByEquipo();
    //List<EquipoElectronico> findAllByOrderByFechaDesc();

    //List<EquipoElectronico> findAllByOrderByCreate_AtDesc();
}
