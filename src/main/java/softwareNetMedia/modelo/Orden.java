package softwareNetMedia.modelo;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.Setter;
import softwareNetMedia.modelo.enums.EstadoFactura;
@Getter
@Setter

@Entity(name = "orden")
@Table(name = "orden")
public class Orden implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date created_at;
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;    
    @Column(length = 15)
    private Long nroFactura;
    @Enumerated(EnumType.STRING)
    private EstadoFactura estado;
    private Double subTotalIva;
    private Double total;
    @Column(length = 36)
    private String external_id;
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(referencedColumnName = "id", name = "id_equipoElectronico")
    private EquipoElectronico equipoElectronico;
    @OneToMany(mappedBy = "orden", cascade = CascadeType.ALL)
    private List<DetalleOrden> detallesOrden;
}
