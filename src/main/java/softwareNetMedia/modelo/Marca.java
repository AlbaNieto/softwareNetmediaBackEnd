package softwareNetMedia.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.Setter;
@Entity(name = "marca")
@Table(name = "marca")
@Getter
@Setter
public class Marca implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date fechaEmision;
    @Column(length = 100)
    private String nombre;
    private Boolean estado;
    @OneToMany(mappedBy = "marca", cascade = CascadeType.ALL)
    private List<EquipoElectronico> equipoElectronicos;
}
