package softwareNetMedia.modelo.enums;
public enum EstadoFactura {
    CANCELADO("CANCELADO"), PAGADO("PAGADO"), ORDEN("ORDEN");
    private String estado;
    private EstadoFactura(String estado){
        this.estado = estado;
    }
    public String getEstado(){
        return estado;
    }
}
