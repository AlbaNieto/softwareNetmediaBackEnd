package softwareNetMedia.modelo.enums;
public enum TipoIdentificacion {
    CEDULA("CEDULA"), RUC("RUC"), PASAPORTE("PASAPORTE");
    private String tipo;
    private TipoIdentificacion(String tipo){
        this.tipo = tipo;
    }
    public String getTipo(){
        return tipo;
    }
}
