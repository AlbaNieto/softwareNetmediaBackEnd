package softwareNetMedia.rest.modelo_rest;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import softwareNetMedia.modelo.Orden;
import softwareNetMedia.modelo.enums.EstadoFactura;

@Getter
@Setter
public class OrdenWS {
    private Long nroFactura;
    private String estado;
    private Double subTotalIva;
    private Double total;
    /**
     * metodo para cargar el correspondiente objeto orden con todos sus datos
     * @param orden
     * @return la orden con todos sus datos ingresados
     */
    public Orden cargarObjeto(Orden orden) {
        if (orden == null) {
            orden = new Orden();
        }
        orden.setNroFactura(nroFactura);
        orden.setSubTotalIva(subTotalIva);
        orden.setTotal(total);
        orden.setExternal_id(UUID.randomUUID().toString());
        switch (estado) {
            case "CANCELADO":
                orden.setEstado(EstadoFactura.CANCELADO);
                break;
            case "PAGADO":
                orden.setEstado(EstadoFactura.PAGADO);
                break;
            case "ORDEN":
                orden.setEstado(EstadoFactura.ORDEN);
                break;
            default:
                break;
        }
        return orden;
    }
}
