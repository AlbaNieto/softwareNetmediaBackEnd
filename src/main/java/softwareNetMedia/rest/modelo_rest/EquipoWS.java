package softwareNetMedia.rest.modelo_rest;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import softwareNetMedia.modelo.EquipoElectronico;

@Getter
@Setter
public class EquipoWS {
    @NotBlank(message = "Campo nombre es requerido")
    @Size(min = 2, max = 75)
    private String nombre;
    private MarcaWS marcaWS;
    private PersonaWS personaWS;
    @NotBlank(message = "Campo de persona es requerido")
    private String external_persona;
    @NotBlank(message = "Campo de marca es requerido")
    private String nombre_marca;
    private OrdenWS ordenWS;

    /**
     * 
     * @param equipo
     * Este método se utiliza para cargar objetos tanto del equipo. si es que está null se crea un nuevo EquipoElectronico
     * @return
     */
    public EquipoElectronico cargarObjeto(EquipoElectronico equipo){
        if(equipo == null){
            equipo  = new EquipoElectronico(); 
        }
        equipo.setNombre(nombre);
        equipo.setExternal_id(UUID.randomUUID().toString());
        return equipo;
    }

}
