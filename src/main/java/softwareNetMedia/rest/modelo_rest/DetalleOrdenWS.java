package softwareNetMedia.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import softwareNetMedia.modelo.DetalleOrden;

public class DetalleOrdenWS {
    private Float pu;
    private Float pt;
    private Integer cantidad;

    public DetalleOrden cargarObjeto(DetalleOrden dOrden) {
        if (dOrden == null) {
            dOrden = new DetalleOrden();
        }
        dOrden.setPu(pu);
        dOrden.setPt(pt);
        dOrden.setCantidad(cantidad);
        dOrden.setExternal_id(UUID.randomUUID().toString());
        
        dOrden.setUpdateAt(new Date());
        return dOrden;
    }
}
