package softwareNetMedia.rest.modelo_rest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import softwareNetMedia.modelo.Marca;

@Getter
@Setter


public class MarcaWS {
    @NotBlank(message = "Campo nombres es requerido")
    @Size(min = 2, max = 75)
    private String nombre;
    private Boolean estado;
/**
 * 
 * @param marca
 * Este método se utiliza para cargar objetos tanto de la marca. si es que está null se crea un nueva marca
 * @return
 */
    public Marca cargarObjeto(Marca marca){
        if (marca == null) {
            marca = new Marca();
        }
        marca.setNombre(nombre);
        marca.setEstado(estado);
        return marca;
    }
}
