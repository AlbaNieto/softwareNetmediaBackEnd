package softwareNetMedia.rest;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.validation.Valid;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import softwareNetMedia.controladores.DetalleRepository;
import softwareNetMedia.controladores.EquipoRepository;
import softwareNetMedia.controladores.OrdenRepository;
import softwareNetMedia.modelo.DetalleOrden;
import softwareNetMedia.modelo.EquipoElectronico;
import softwareNetMedia.modelo.Orden;
import softwareNetMedia.modelo.Persona;
import softwareNetMedia.modelo.enums.EstadoFactura;
import softwareNetMedia.rest.modelo_rest.DetalleOrdenWS;
import softwareNetMedia.rest.modelo_rest.OrdenWS;
import softwareNetMedia.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = { RequestMethod.GET, RequestMethod.POST,
        RequestMethod.HEAD })


public class OrdenController {
    @Autowired
    private OrdenRepository ordenRepository;
    @Autowired
    private EquipoRepository equipoRepository;
    @Autowired
    private DetalleRepository detalleRepository;


    @GetMapping("/ordenes/buscar/{fecha}/{estado}")
    /**
     * Buscar ordenes por estado en una fecha determinada
     * @param estado
     * @param fecha
     * @return lista de ordenes del correspondiente cliente
     */
    public ResponseEntity buscarOrdenes(@PathVariable Date fecha, @PathVariable String estado) {
        List<Orden> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        if (estado == "CANCELADO") {
            ordenRepository.findByEstado(EstadoFactura.CANCELADO).forEach((p) -> lista.add(p));
        } else if (estado == "PAGADO") {
            ordenRepository.findByEstado(EstadoFactura.PAGADO).forEach((p) -> lista.add(p));
        } else {
            ordenRepository.findByEstado(EstadoFactura.ORDEN).forEach((p) -> lista.add(p));
        }
        for (Orden p : lista) {
            if (fecha.before(p.getFechaEmision())) {
                HashMap aux = new HashMap<>();
                aux.put("creado en", p.getFechaEmision());
                aux.put("nroFactura", p.getNroFactura());
                aux.put("estado", p.getEstado());
                aux.put("subTotalIva", p.getSubTotalIva());
                aux.put("total", p.getTotal());
                aux.put("external_id", p.getExternal_id());
                aux.put("equipoElectronico", p.getEquipoElectronico().getNombre());
                aux.put("cliente", p.getEquipoElectronico().getPersona().getNombres());
                mapa.add(aux);
            }

        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @GetMapping("/ordenes/buscarIdentificacion/{identificacion}")
    /**
     * Buscar ordenes por identificacion del cliente
     * 
     * @param identificacion
     * @return lista de ordenes del correspondiente cliente
     */
    public ResponseEntity buscarOrdenesIdentificacion(@PathVariable String identificacion) {
        List<Orden> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        ordenRepository.buscarPorIdentificacion(identificacion).forEach((p) -> lista.add(p));
        for (Orden p : lista) {
            HashMap aux = new HashMap<>();
            aux.put("creado en", p.getFechaEmision());
            aux.put("nroFactura", p.getNroFactura());
            aux.put("estado", p.getEstado());
            aux.put("subTotalIva", p.getSubTotalIva());
            aux.put("total", p.getTotal());
            aux.put("external_id", p.getExternal_id());
            aux.put("equipoElectronico", p.getEquipoElectronico().getNombre());
            aux.put("cliente", p.getEquipoElectronico().getPersona().getNombres());
            mapa.add(aux);

        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @GetMapping("/ordenes/buscarNombre/{nombre}")
    /**
     * Buscar ordenes por el nombre del cliente
     * 
     * @param nombre
     * @return lista de ordenes del correspondiente cliente
     */
    public ResponseEntity buscarOrdenesNombre(@PathVariable String nombre) {
        List<Orden> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        ordenRepository.buscarPorNombres(nombre).forEach((p) -> lista.add(p));
        for (Orden p : lista) {
            HashMap aux = new HashMap<>();
            aux.put("creado en", p.getFechaEmision());
            aux.put("nroFactura", p.getNroFactura());
            aux.put("estado", p.getEstado());
            aux.put("subTotalIva", p.getSubTotalIva());
            aux.put("total", p.getTotal());
            aux.put("external_id", p.getExternal_id());
            aux.put("equipoElectronico", p.getEquipoElectronico());
            aux.put("cliente", p.getEquipoElectronico().getPersona().getNombres());
            mapa.add(aux);

        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * Método para guardar orden teniendo en cuenta el external_id del equipo_electronico
     * @return
     */ 
    @PostMapping("/orden/guardar")
    public ResponseEntity guardarOrden(@Valid @RequestBody OrdenWS ordenWS) {
        Double suma = 0.0;
        HashMap mapa = new HashMap<>();
        Orden orden = new Orden();
        List<DetalleOrden> lista = new ArrayList<>();
        EquipoElectronico p = equipoRepository.findByExternal_idAlt(ordenWS.getExternal_equipo());
        detalleRepository.findAll().forEach((e) -> lista.add(e));
        for (DetalleOrden e : lista) {
            HashMap aux = new HashMap<>();
            aux.put("identificacion", e.getPt());
            System.out.println(e.getPt());
            suma = suma + e.getPt();
        }
        if (orden != null) {
            orden.setFechaEmision(new Date());
            Long nroFactura = Math.abs(new Random().nextLong()) % 1000000000L;
            while (ordenRepository.findByNroFactura(nroFactura) != null) {
                nroFactura = Math.abs(new Random().nextLong()) % 1000000000L;
            }
            orden.setExternal_id(UUID.randomUUID().toString());
            orden.setNroFactura(nroFactura);
            orden.setEstado(EstadoFactura.ORDEN);
            orden.setSubTotalIva(suma);
            orden.setTotal(suma*1.12);
            orden.setEquipoElectronico(p);
            ordenRepository.save(orden);
            mapa.put("evento", "Se ha registrado correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        } else

        {
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el tipo de persona deseado");
        }
    }



    /**
     * Obtener una orden por su external id.
     * @param external External id de la orden.
     * @return Retorna una respuesta con los datos del cliente y detalles de la orden.
     */
    @CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
    @GetMapping("/ordenes/{external}")
    public ResponseEntity obtenerOrden(@PathVariable String external) {
        Orden orden = ordenRepository.findByExternal_id(external);
        if (orden != null) {
            HashMap aux = new HashMap<>();
            List detalles = new ArrayList<>();
            Persona persona = orden.getEquipoElectronico().getPersona();
            aux.put("identificacion",persona.getIdentificacion());
            aux.put("nombres", persona.getNombres());
            aux.put("apellidos",persona.getApellidos());
            aux.put("direccion", persona.getDireccion());
            aux.put("telefono",persona.getTelefono());
            for(DetalleOrden d: orden.getDetallesOrden()){
                HashMap detalle = new HashMap<>();
                detalle.put("cantidad", d.getCantidad());
                detalle.put("descripcion",d.getServicio().getNombre());
                detalle.put("pu",d.getPu());
                detalle.put("pt",d.getPt());
                detalles.add(detalle);
            }
            aux.put("nroFactura",orden.getNroFactura());
            aux.put("fechaEmision", orden.getFechaEmision().toString());
            aux.put("dispositivo", orden.getEquipoElectronico().getNombre());
            aux.put("subtotal",orden.getSubTotalIva());
            aux.put("total",orden.getTotal());
            aux.put("detalles", detalles);
            return RespuestaLista.respuestaLista(aux);
        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Orden no encontrada");
            return RespuestaLista.respuesta(mapa, "No se encontro la orden deseada");            
        }
    }

    /**
     * Obtener una lista de ordenes entre un rango de fechas.
     * @param fechaIni Fecha inicial del rango.
     * @param fechaFin Fecha final del rango
     * @return Respuesta con la lista de ordenes
     */
    @CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
    @GetMapping("/ordenes/buscarF/{fechaIni}/{fechaFin}")
    public ResponseEntity buscarFecha(@PathVariable String fechaIni, @PathVariable String fechaFin){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy",Locale.ENGLISH);
        Date fechaInicio;
        Date fechaF;
        try{
            fechaInicio = formatter.parse(fechaIni);
            fechaF = formatter.parse(fechaFin);
        }catch(ParseException e){
            return RespuestaLista.respuestaError(e, "Error de fecha");
        }
        List<Orden> ordenes = new ArrayList<>();
        List mapa = new ArrayList<>();
        ordenRepository.findByFechaEmisionBetween(fechaInicio,fechaF).forEach((o)-> ordenes.add(o));
        for (Orden o: ordenes){
            HashMap aux = new HashMap<>();
            aux.put("cliente", o.getEquipoElectronico().getPersona().getNombres()+" "+o.getEquipoElectronico().getPersona().getApellidos());
            aux.put("nroFactura",o.getNroFactura());
            aux.put("estado", o.getEstado());
            aux.put("subTotalIva", o.getSubTotalIva());
            aux.put("total", o.getTotal());
            aux.put("equipoElectronico", o.getEquipoElectronico().getNombre());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * Obtener una lista de todas las ordenes
     * @return Respuesta con todas las ordenes.  
     */
    @GetMapping("/ordenes/listar")
    public ResponseEntity listar() {
        List<Orden> ordenes = new ArrayList<>();
        List mapa = new ArrayList<>();
        ordenRepository.findAll().forEach((o) -> ordenes.add(o));
        for (Orden o : ordenes) {
            HashMap aux = new HashMap<>();
            aux.put("nroFactura", o.getNroFactura());
            aux.put("cliente", o.getEquipoElectronico().getPersona().getNombres()+" "+o.getEquipoElectronico().getPersona().getApellidos());
            aux.put("dispositivo", o.getEquipoElectronico().getNombre());
            aux.put("external", o.getExternal_id());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }
}
