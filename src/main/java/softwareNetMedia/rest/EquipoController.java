package softwareNetMedia.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import softwareNetMedia.controladores.EquipoRepository;
import softwareNetMedia.controladores.MarcaRepository;
import softwareNetMedia.controladores.OrdenRepository;
import softwareNetMedia.controladores.PersonaRepository;
import softwareNetMedia.modelo.DetalleOrden;
import softwareNetMedia.modelo.EquipoElectronico;
import softwareNetMedia.modelo.Marca;
import softwareNetMedia.modelo.Orden;
import softwareNetMedia.modelo.Persona;
import softwareNetMedia.modelo.enums.EstadoFactura;
import softwareNetMedia.rest.modelo_rest.EquipoWS;
import softwareNetMedia.rest.modelo_rest.OrdenWS;
import softwareNetMedia.rest.modelo_rest.PersonaWS;
import softwareNetMedia.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = { RequestMethod.GET, RequestMethod.POST,
        RequestMethod.HEAD })
public class EquipoController {

    @Autowired
    private EquipoRepository equipoRepository;

    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private MarcaRepository marcaRepository;
    PersonaWS personaWS;
    /**
     * Método para guardar equipos electronicos con toda su información
     * @param equipoWS
     * @return
     */
    @CrossOrigin("http://localhost:3006/")
    @PostMapping("/equipos/guardar")
    public ResponseEntity guardarEquipo(@Valid @RequestBody EquipoWS equipoWS) {
        HashMap mapa = new HashMap<>();
        EquipoElectronico equipo = new EquipoElectronico();
        Persona p = personaRepository.findByExternal_id(equipoWS.getExternal_persona());
        Marca marca = marcaRepository.findByNombre_Marca(equipoWS.getNombre_marca());
        if (p != null) {
            equipo.setCreateAt(new Date());
            equipo.setPersona(p);
            equipo.setMarca(marca);
            if (marca != null) {
                equipoWS.cargarObjeto(equipo);
            } else {
                mapa.put("evento", "Objeto no encontrado");
                return RespuestaLista.respuesta(mapa, "No se encontro la marca del equipo");
            }
            equipoRepository.save(equipo);
            mapa.put("evento", "Se ha registrado correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        } else {
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa,
                    "No se encontro el tipo de persona deseado");
        }
    }
    /**
     * Método para listar todos los datos de equipos electronicos, asi como de marca que va en equipoelectronico
     * @return
     */
    @CrossOrigin("http://localhost:3006/")
    @GetMapping("/equipos")
    public ResponseEntity listarEquipos() {
        List<EquipoElectronico> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        equipoRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (EquipoElectronico p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            // aux.put("id_persona", p.getPersona().getExternal_id());
            // aux.put("persona", p.getPersona().getApellidos() + " " +
            // p.getPersona().getNombres());
            // aux.put("identificacion", p.getPersona().getIdentificacion());
            Persona persona = p.getPersona();
            if (persona != null && persona.getNombres() != null) {
                aux.put("nombre", persona.getNombres());
                aux.put("apellidos", persona.getApellidos());
                aux.put("id_persona", persona.getExternal_id());
            }
            aux.put("Equipo Electronico", p.getNombre());
            aux.put("Equipo_external", p.getExternal_id());
            Marca marca = p.getMarca();
            if (marca != null && marca.getNombre() != null) {
                aux.put("marca", marca.getNombre());
            }

            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }
    /**
     * Método para listar toda la información importante de marca, se lo utiliza para registrar equipo electronico
     * @return
     */
    @CrossOrigin("http://localhost:3006/")
    @GetMapping("/marcas")
    public ResponseEntity listarMarcas() {
        List<Marca> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        marcaRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Marca p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("marca", p.getNombre());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    /**
     * este método se lo utiliza para obtener equipos electronico de acuerdo a su external ID
     * @param external
     * @return
     */
    @GetMapping("/equipos/obtener/{external}")
    public ResponseEntity listarEquiposId(@PathVariable String external) {
        // Persona pe = personaRepository.findByExternal_id(external);
        List<EquipoElectronico> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        equipoRepository.findByExternal_id(external).forEach((p) -> lista.add(p));
        // equipoRepository.findByPersona(pe).forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (EquipoElectronico p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("id_persona", p.getPersona().getExternal_id());
            aux.put("persona", p.getPersona().getApellidos() + " " + p.getPersona().getNombres());
            aux.put("identificacion", p.getPersona().getIdentificacion());
            aux.put("Equipo Electronico", p.getNombre());
            aux.put("Marca", p.getMarca().getNombre());

            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

}
